package com.example.lab9_f

import retrofit2.Call
import retrofit2.http.*

interface JsonPlaceholderAPI {
    @GET("shoutbox/messages")
    fun getExampleItemArray(): Call<ArrayList<ExampleItem>?>?

    @POST("shoutbox/message")
    fun createPost(@Body exampleItem: ExampleItem): Call<ExampleItem>

    @PUT("shoutbox/message/{id}")
    fun createPut(
        @Path("id") id:String,
        @Body exampleItem: ExampleItem):Call<ExampleItem>
    @DELETE("shoutbox/message/{id}")
    fun createDelete(
        @Path("id") id:String
    ):Call<ExampleItem>
}