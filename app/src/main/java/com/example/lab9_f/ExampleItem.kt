package com.example.lab9_f

import java.io.Serializable


class ExampleItem : Serializable {
    var content: String? = null
    var login: String? = null
    var date: String? = null
    var id: String? = null


    constructor(log: String?, content: String?) {
        this.login = log
        this.content = content
    }
}