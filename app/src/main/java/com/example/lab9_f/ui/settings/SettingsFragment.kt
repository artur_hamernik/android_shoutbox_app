package com.example.lab9_f.ui.settings

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.example.lab9_f.R
import com.example.lab9_f.ui.shoutbox.ShoutboxFragment


class SettingsFragment : Fragment() {
    private lateinit var editText:EditText
    private lateinit var button:Button
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        button = root.findViewById(R.id.button)
        editText = root.findViewById(R.id.editLogin)
        readData()
        button.setOnClickListener {
            if(verifyAvailableNetwork(activity as AppCompatActivity)){
                saveData()
                val bundle = Bundle()
                bundle.putString("login", editText.text.toString())
                val fragment: Fragment = ShoutboxFragment()
                fragment.arguments = bundle
                val fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()
                    ?.replace(R.id.nav_host_fragment, fragment)
                    ?.commit()
            }
            else {
                Toast.makeText(context, "No internet connection!",Toast.LENGTH_SHORT).show()
            }

        }
        return root
    }
    private fun saveData(){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.saved_login), editText.text.toString())
            commit()
        }
    }
    private fun readData(){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = resources.getString(R.string.login)
        editText.setText(sharedPref.getString(getString(R.string.saved_login), defaultValue))
    }
    private fun verifyAvailableNetwork(activity: AppCompatActivity):Boolean{
        val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }
}