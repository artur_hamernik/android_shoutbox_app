package com.example.lab9_f.ui.edit

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.lab9_f.ExampleItem
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.ui.shoutbox.ShoutboxFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class EditFragment : Fragment(){

    private lateinit var textLogin:TextView
    private lateinit var textDate:TextView
    private lateinit var textHour:TextView
    private lateinit var textContent:EditText
    private lateinit var deleteButton:ImageView
    private lateinit var button: Button
    private  lateinit var login:String
    private  lateinit var date:String
    private  lateinit var content:String
    private  lateinit var id:String
    private lateinit var jsonPlaceholderAPI:JsonPlaceholderAPI

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_edit, container, false)

        login = arguments?.getString("login").toString()
        date = arguments?.getString("date_hour").toString()
        id = arguments?.getString("id").toString()
        content = arguments?.getString("content").toString()

        textLogin = root.findViewById(R.id.loginE)
        textDate = root.findViewById(R.id.dateE)
        textHour = root.findViewById(R.id.hourE)
        textContent = root.findViewById(R.id.contentE)
        button = root.findViewById(R.id.edit_button)
        deleteButton = root.findViewById(R.id.deleteButton)

        textLogin.text = login
        textDate.text = date.subSequence(0,10)
        textHour.text = date.subSequence(11,19)
        textContent.setText(content)

        val baseUrl = "http://tgryl.pl/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)

        button.setOnClickListener {
            if(verifyAvailableNetwork(activity as AppCompatActivity)) {
                content = textContent.text.toString()
                createPut()
                val bundle = Bundle()
                bundle.putString("login", login)
                val fragment: Fragment = ShoutboxFragment()
                fragment.arguments = bundle
                val fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()
                    ?.replace(R.id.nav_host_fragment, fragment)
                    ?.commit()
            }
            else{
                Toast.makeText(context, "No internet connection!",Toast.LENGTH_SHORT).show()
            }
        }
        deleteButton.setOnClickListener {
            if(verifyAvailableNetwork(activity as AppCompatActivity)) {
                createDelete(id)
                val bundle = Bundle()
                bundle.putString("login", login)
                val fragment: Fragment = ShoutboxFragment()
                fragment.arguments = bundle
                val fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()
                    ?.replace(R.id.nav_host_fragment, fragment)
                    ?.commit()
            }
            else{
            Toast.makeText(context, "No internet connection!",Toast.LENGTH_SHORT).show()
            }
        }

        return root
    }
    private fun createDelete(id:String){
        val call = jsonPlaceholderAPI.createDelete(id)
        call.enqueue(object : Callback<ExampleItem> {
            override fun onResponse(
                call: Call<ExampleItem>,
                response: Response<ExampleItem>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
            }
            override fun onFailure(
                call: Call<ExampleItem>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
    private fun createPut(){
        val exampleItem = ExampleItem(login,content)
        val call = jsonPlaceholderAPI.createPut(id,exampleItem)
        call.enqueue(object : Callback<ExampleItem> {
            override fun onResponse(
                call: Call<ExampleItem>,
                response: Response<ExampleItem>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
            }
            override fun onFailure(
                call: Call<ExampleItem>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }

    private fun verifyAvailableNetwork(activity: AppCompatActivity):Boolean{
        val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }
}