package com.example.lab9_f.ui.shoutbox

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.lab9_f.ExampleAdapter
import com.example.lab9_f.ExampleItem
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.ui.edit.EditFragment
import com.example.lab9_f.SwipeToDeleteCallBack
import kotlinx.android.synthetic.main.fragment_shoutbox.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class ShoutboxFragment : Fragment(), ExampleAdapter.OnItemClickListener{

    private lateinit var login:String
    private  lateinit var eim:ArrayList<ExampleItem>
    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    val e = Executors.newSingleThreadScheduledExecutor()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_shoutbox, container, false)
        val textView = root.findViewById<TextView>(R.id.editContent)
        val imageButton = root.findViewById<ImageView>(R.id.imageButton)

        val baseUrl = "http://tgryl.pl/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)

        login = arguments?.getString("login").toString()
        createGet()

        swipeRefreshLayout = root.findViewById(R.id.swipe_refresh)
        swipeRefreshLayout.setOnRefreshListener {
            val handler = Handler()
            handler.postDelayed({
                if(verifyAvailableNetwork(activity as AppCompatActivity)) {
                    createGet()
                    Toast.makeText(context, "Data refreshed!", Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(context, "No internet connection!",Toast.LENGTH_SHORT).show()
                }
                if (swipeRefreshLayout.isRefreshing) {
                    swipeRefreshLayout.isRefreshing = false
                }
            }, 1000)
        }

        e.scheduleAtFixedRate({
            createGet()
        }, 30, 30, TimeUnit.SECONDS)

        imageButton.setOnClickListener {
            if(verifyAvailableNetwork(activity as AppCompatActivity)) {
                createPost(login, textView)
            }
            else{
                Toast.makeText(context, "No internet connection!",Toast.LENGTH_SHORT).show()
            }
        }
        return root
    }
    private fun setSwipe(){
        if(verifyAvailableNetwork(activity as AppCompatActivity)) {
            val swipeHandler = object : SwipeToDeleteCallBack(context) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val adapter = recycler_view.adapter as ExampleAdapter
                    if(adapter.checkLogin(viewHolder.adapterPosition) == login){
                        adapter.removeAt(viewHolder.adapterPosition)?.let { createDelete(it) }
                    }
                    else{
                        createGet()
                    }

                }
            }
            val itemTouchHelper = ItemTouchHelper(swipeHandler)
            itemTouchHelper.attachToRecyclerView(recycler_view)
            val toast = Toast.makeText(context,"Data refreshed!", Toast.LENGTH_SHORT)
            toast.show()
        }
        else{
            Toast.makeText(context, "No internet connection!",Toast.LENGTH_SHORT).show()
        }
    }
    fun updateData(){
        recycler_view.adapter = ExampleAdapter(eim,this)
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.setHasFixedSize(true)
        setSwipe()

    }
    fun createGet(){
        val call = jsonPlaceholderAPI.getExampleItemArray()
        call!!.enqueue(object : Callback<ArrayList<ExampleItem>?> {
            override fun onResponse(
                call: Call<ArrayList<ExampleItem>?>,
                response: Response<ArrayList<ExampleItem>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                eim = response.body()!!
                eim.reverse()
                updateData()
            }

                override fun onFailure(
                    call: Call<ArrayList<ExampleItem>?>,
                    t: Throwable
                ) {
                    println(t.message)
                }
            })
    }
    private fun createPost(log: String?, textView: TextView){
            val exampleItem = ExampleItem(log, textView.text.toString())

            val call = jsonPlaceholderAPI.createPost(exampleItem)

            call.enqueue(object : Callback<ExampleItem> {
                override fun onResponse(
                    call: Call<ExampleItem>,
                    response: Response<ExampleItem>
                ) {
                    if (!response.isSuccessful) {
                        println("Code: " + response.code())
                        return
                    }
                    createGet()
                }

                override fun onFailure(
                    call: Call<ExampleItem>,
                    t: Throwable
                ) {
                    println(t.message)
                }
            })
    }
    private fun createDelete(id:String){
        val call = jsonPlaceholderAPI.createDelete(id)
        call.enqueue(object : Callback<ExampleItem> {
            override fun onResponse(
                call: Call<ExampleItem>,
                response: Response<ExampleItem>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val temp = response.body()
                createGet()
            }

                override fun onFailure(
                    call: Call<ExampleItem>,
                    t: Throwable
                ) {
                    println(t.message)
                }
            })
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("saved_login", login)
    }

    override fun onDetach() {
        super.onDetach()
        e.shutdown()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            login = savedInstanceState.getString("saved_login").toString()
        }
    }

    override fun onItemClick(item: ExampleItem, position: Int) {
        if(verifyAvailableNetwork(activity as AppCompatActivity)) {
            if (login == item.login) {
                val bundle = Bundle()
                bundle.putString("login", item.login)
                bundle.putString("id", item.id)
                bundle.putString("date_hour", item.date)
                bundle.putString("content", item.content)
                val fragment: Fragment = EditFragment()
                fragment.arguments = bundle
                val fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()
                    ?.replace(R.id.nav_host_fragment, fragment)
                    ?.commit()
            } else {
                Toast.makeText(
                    context,
                    "You can't edit someone else's messages",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        else{
            Toast.makeText(context, "No internet connection!",Toast.LENGTH_SHORT).show()
        }
    }
    private fun verifyAvailableNetwork(activity: AppCompatActivity):Boolean{
        val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }

}