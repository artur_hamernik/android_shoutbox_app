package com.example.lab9_f

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.example_item.view.*

class ExampleAdapter(private var exampleList: ArrayList<ExampleItem>,var clickListener: OnItemClickListener): RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.example_item,
            parent, false)

        return ExampleViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        holder.initialize(exampleList[position],clickListener)

    }
    override fun getItemCount() = exampleList.size

    class ExampleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var textText: TextView = itemView.textText
        var textLog: TextView = itemView.textLog
        var textDate: TextView = itemView.textDate
        var textHour: TextView = itemView.textHour
        fun initialize(item: ExampleItem, action: OnItemClickListener){
            textText.text = item.content
            textLog.text = item.login
            textDate.text = item.date.toString().subSequence(0,10)
            textHour.text = item.date.toString().subSequence(11,19)

            itemView.setOnClickListener {
                action.onItemClick(item, adapterPosition)
            }
        }
    }
    fun removeAt(position: Int): String? {
        val id = exampleList[position].id
        exampleList.removeAt(position)
        notifyItemRemoved(position)

        return id;
    }
    fun checkLogin(position: Int):String?{
        return exampleList[position].login
    }

    interface OnItemClickListener{
        fun onItemClick(item: ExampleItem, position:Int)
    }
}